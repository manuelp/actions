(defproject actions "1.0.1"
  :description "A simple actions manager with the todo.txt plain text format."
  :dependencies [[org.clojure/clojure "1.3.0"]]
  :dev-dependencies [[lein-marginalia "0.7.0"]]
  :main actions.console)
